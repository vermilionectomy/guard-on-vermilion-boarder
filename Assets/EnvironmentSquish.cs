﻿using UnityEngine;
using System.Collections;

public class EnvironmentSquish : MonoBehaviour {

    public GameObject squishEffect;
    public GameObject bloodSquirt;
    public bool knockDown;

    AudioSource m_audio;

    public AudioClip[] squish; 

    void Awake()
    {
        m_audio = GetComponent<AudioSource>(); 
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            for(int i = 0; i < 5; i++)
            {
                Instantiate(squishEffect, transform.position, Quaternion.Euler(0.0f, 0.0f, Random.Range(0.0f, 360.0f)));
                Instantiate(bloodSquirt, transform.position, Quaternion.identity);
                m_audio.pitch += .1f; 
                int randNum = Random.Range(0, squish.Length);
                m_audio.PlayOneShot(squish[randNum], 0.05f);
                if (knockDown)
                {
                    other.GetComponent<CharacterController2D>().Knockdown();
                }
            }
        }
    }
}
