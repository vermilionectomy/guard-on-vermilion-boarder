﻿using UnityEngine;
using System.Collections;

public class EndNarration : MonoBehaviour {

    public GameObject narrateCanvas;

    CharacterController2D player;

    public bool gameStartNow;

    public GameObject button1;
    public GameObject button2;

    float timer; 

    void Start()
    {
        player = FindObjectOfType<CharacterController2D>(); 
    }

    void Update()
    {
        timer += Time.deltaTime;
        if(timer >= 4)
        {
            button1.SetActive(true);
            button2.SetActive(true); 
        }
        else
        {
            button1.SetActive(false);
            button2.SetActive(false); 
        }
    }
    public void OnClick_Yes()
    {
        gameStartNow = true; 
        player.enabled = true; 
        Destroy(narrateCanvas.gameObject); 
    }
}
