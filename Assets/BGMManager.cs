﻿using UnityEngine;
using System.Collections;

public class BGMManager : MonoBehaviour
{

    public AudioClip[] music;
    AudioSource m_audio;
    AudioClip m_clip;
    GameManager game;
    bool hasChanged;
    
    void Start()
    {
        m_audio = GetComponent<AudioSource>();
        game = FindObjectOfType<GameManager>();
        m_clip = music[0];
    }


    void Update()
    {
        
        
        //if (game.remainingWalls == 3)
        //{
        //    if (hasChanged)
        //    {
        //        m_audio.volume = Mathf.Lerp(m_audio.volume, 0, (Time.deltaTime));
        //
        //    }
        //    m_audio.PlayOneShot(music[1], .5f);
        //}
        //else
        //{
        //    m_audio.PlayOneShot(music[0], .5f);
        //}
        //if(!m_audio.isPlaying)
        //    m_audio.PlayOneShot(m_clip,0.5f);       

        if (game.remainingWalls <= 3)
        {            
            
           if(!hasChanged)
            {
                m_audio.clip = music[1];
                m_audio.Play(); 
                //m_audio.PlayOneShot(music[1], 0.5f);
                hasChanged = true; 
            }           
        }

        else
        {

        }

    }

    //IEnumerator Fade()
    //{
    //    hasChanged = false;
    //    while (m_audio.volume > 0.1f)
    //    {
    //        m_audio.volume = Mathf.Lerp(m_audio.volume, 0, 0.3f*Time.deltaTime);
    //    }

    //    m_audio.volume = 0.0f;
    //    yield return new WaitForSeconds(0.1f);
    //    m_clip = music[1];

    //    while (m_audio.volume < 0.4f)
    //    {
    //        m_audio.volume = Mathf.Lerp(m_audio.volume, 0, 0.3f*Time.deltaTime);
    //    }
    //    m_audio.volume = 0.5f;

    //}


}
