﻿using UnityEngine;
using System.Collections;

public class ObjectLifetime : MonoBehaviour
{
    public float lifeTime; 

	void Update ()
    {
        GameObject.Destroy(this.gameObject, lifeTime); 
	}
}
