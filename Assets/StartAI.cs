﻿using UnityEngine;
using System.Collections;

public class StartAI : MonoBehaviour
{

    EndNarration narrate;


    Animator m_anim;
    bool hasDone;

    float timer;

    void Start()
    {
        m_anim = GetComponent<Animator>();
        narrate = FindObjectOfType<EndNarration>();
    }

    void Update()
    {
        if (narrate.gameStartNow)
        {
            if (!hasDone)
            {
                m_anim.Play("Run Away");
                hasDone = true;
            }
        }

        if(hasDone)
        {
            timer += Time.deltaTime;
            if(timer >= 2)
            {
                Destroy(this.gameObject); 
            }
        }
    }
}
