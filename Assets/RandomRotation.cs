﻿using UnityEngine;
using System.Collections;

public class RandomRotation : MonoBehaviour {

    void Start()
    {
        float ranSplatterRotate = Random.Range(-360, 360);
        transform.rotation = new Quaternion(transform.rotation.x, transform.rotation.y, ranSplatterRotate, 0);
    }
}
