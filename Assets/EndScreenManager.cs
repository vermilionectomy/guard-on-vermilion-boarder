﻿using UnityEngine;
using System.Collections;

public class EndScreenManager : MonoBehaviour {

    public int sceneToLoad; 

	public void OnClick_Menu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(sceneToLoad); 
    }
}
