﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour
{
    public enum States
    {
        full,
        damage1,
        damage2,
        destroyed
    }

    public Sprite[] wallSprite; 

    public States m_state;

    public Target target;

    public float lifetimeMax;
    float lifetime;

    public bool underAttack;
    public int enemyCount;

    SpriteRenderer m_sprite;

    public GameObject sparkEffect;
    public GameObject fireEffect;
    public GameObject destroyedEffect; 
    bool spawnedSparkEffect;

    public Transform targetposition;
    
    // Use this for initialization
    void Start()
    {
        m_sprite = GetComponent<SpriteRenderer>(); 
        target = GetComponentInChildren<Target>();
        lifetime = lifetimeMax;

        targetposition = GetComponentInChildren<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (m_state)
        {
            case States.full:
                target.targetable = true;
                m_sprite.sprite = wallSprite[0]; 
                break;
            case States.damage1:
                target.targetable = true;

                if(!spawnedSparkEffect)
                {
                    Instantiate(sparkEffect, transform.position, Quaternion.identity);
                    spawnedSparkEffect = true; 
                }
              
                m_sprite.sprite = wallSprite[1];
                break;
            case States.damage2:
                target.targetable = true;

                if(spawnedSparkEffect)
                {
                    Instantiate(fireEffect, transform.position, transform.rotation);
                    spawnedSparkEffect = false; 
                }
                m_sprite.sprite = wallSprite[2];
                break;
            case States.destroyed:
                
                if(!spawnedSparkEffect)
                {
                    Instantiate(destroyedEffect, transform.position, transform.rotation);
                    spawnedSparkEffect = true; 
                }
                Destroy(this.gameObject);
                for (var i = GameManager.Instance.m_wallTargets.Count - 1; i > -1; i--)
                {
                    if (GameManager.Instance.m_wallTargets[i] == null)
                        GameManager.Instance.m_wallTargets.RemoveAt(i);
                }
                target.targetable = false;
                break;
        }

        if (underAttack)
        {
            lifetime -= Time.deltaTime * enemyCount;
            if (lifetime < 0.1f)
            {
                m_state++;
                if (m_state == States.destroyed)
                {
                    GameManager.Instance.remainingWalls--;
                    return;
                }
                lifetime = lifetimeMax;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.CompareTag("Enemy"))
        {
            if (other.gameObject.GetComponent<Enemy>().m_target == target)
            {
                other.gameObject.GetComponent<Enemy>().attackingWall = true;
                other.gameObject.GetComponent<Enemy>().wall = this;
                underAttack = true;
                enemyCount++;
            }
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            if (other.gameObject.GetComponent<Enemy>().m_target == target)
            {
                other.gameObject.GetComponent<Enemy>().attackingWall = false;
                enemyCount--;
                if(enemyCount <= 0)
                {
                    underAttack = false;
                }
            }
        }
    }
}
