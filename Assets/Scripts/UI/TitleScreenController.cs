﻿using UnityEngine;
using System.Collections;

public class TitleScreenController : MonoBehaviour
{
    [SerializeField]
    private int sceneToLoad;

    float timer;
    bool pressedStart;
    bool hasSpawned; 
    public GameObject smearEffect;
    public Transform buttontransform;

    public AudioClip squish;
    AudioSource m_audio; 

    float waitTimer; 

    void Start()
    {
        m_audio = GetComponent<AudioSource>(); 
    }
    void Update()
    {
        if(pressedStart)
        {
            timer += Time.deltaTime;
            if(!hasSpawned)
            {
                //GameObject Smear = Instantiate(smearEffect, buttontransform.transform.position,transform.rotation) as GameObject;
                //Smear.transform.parent = this.transform;                       
               // hasSpawned = true; 
            }                 
               
            
        }
        
    }
    public void OnClick_Start()
    {
        pressedStart = true;
        m_audio.PlayOneShot(squish, .6f); 
        UnityEngine.SceneManagement.SceneManager.LoadScene(sceneToLoad);
    }

    public void OnClick_Quit()
    {
        Application.Quit(); 
    }

  
}
