﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; 

public class UIController : MonoBehaviour
{
    public Text deathCounterText;
    public Text waveCounterText;

    public Image prompt;
    public Image wasdaPrompt; 
    CharacterController2D player;

    GameManager game; 

    void Awake()
    {
        game = FindObjectOfType<GameManager>(); 
        player = FindObjectOfType<CharacterController2D>();
        deathCounterText.text = "Remaining Walls :" + game.remainingWalls.ToString(); 
        waveCounterText.text = "Wave " + game.waveNum.ToString(); 
    }  
	
	void Update ()
    {
        if(player.isPlayerDown)
        {
            prompt.enabled = true;
        }
        else
        {
            prompt.enabled = false; 
        }

        if(player.isSlowed)
        {
            wasdaPrompt.enabled = true;
        }
        else
        {
            wasdaPrompt.enabled = false; 
        }
        deathCounterText.text = "Remaining Walls : " + game.remainingWalls.ToString();
        waveCounterText.text = "Wave " + game.waveNum.ToString();
    }
}
