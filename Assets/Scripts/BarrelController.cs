﻿using UnityEngine;
using System.Collections;

public class BarrelController : MonoBehaviour {

    public int health;
    public GameObject explosion;
    private GameObject e;
    public ParticleSystem expParticle;
    private ParticleSystem ep;
    public ParticleSystem smoke1;
    public ParticleSystem smoke2;
    private ParticleSystem s1;
    private ParticleSystem s2;

    void Start ()
    {
        
    }
	
	void Update ()
    {
	    if (health <= 0)
        {
            e = Instantiate(explosion, transform.position, Quaternion.identity) as GameObject;
            ep = Instantiate(expParticle, transform.position, Quaternion.identity) as ParticleSystem;
            ep.transform.SetParent(e.transform);
            ep.Emit(1);
            Destroy(this.gameObject);
        }

	}

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Bullet"))
        {
            health--;
            if (health == 2)
            {
                s1 = Instantiate(smoke1, transform.position, Quaternion.identity) as ParticleSystem;
                s1.transform.SetParent(this.transform);
            }
            else
            {
                if (health == 1)
                {
                    s2 = Instantiate(smoke2, transform.position, Quaternion.identity) as ParticleSystem;
                    s2.transform.SetParent(this.transform);
                }
            }
        }
    }
}
