﻿using UnityEngine;
using System.Collections;

//Current Weapons 
public enum weapons
{
    Machinegun,
    Shotgun,
    Rocket,
}

public class CharacterController2D : MonoBehaviour
{
    public bool canBeHit;
    public float playerMoveSpeed;

    public GameObject bullet;


    public Weapon shotgun;
    public Weapon rocket;
    public Weapon machinegun;

    private int bullets;
    private Vector2 mousePos;
    private Vector2 mouseAng;
    private float nextFire;
    public float fireRate;
    public float mfireRate;
    public float sfireRate;
    public float rfireRate;

    public float pickUpTimer;

    public bool canMove;
    public int buttonMashCount;
    public int wasdCount;

    public Target target;

    public int slowEnemyCount;

    public weapons weaponStates;

    AudioSource m_Audio;


    public AudioClip machineGunSound;
    public AudioClip shotgunSound;
    public AudioClip rocketSound;
    public AudioClip playerHitSound;
    float footstepTimer;

    public AudioClip[] footstepSounds;
    int footstepNum;
    Rigidbody2D rb;

    CameraScreenShake shake;

    public SpriteRenderer m_sprite;
    public bool isPlayerFlipped;
    public bool isPlayerDown;
    public GameObject getUpParticle;
    Animator m_anim;

    public GameObject muzzleFlash;

    public Sprite mBullet;
    public Sprite sBullet;
    public Sprite rBullet;

    public float mBulletScale;
    public float sBulletScale;
    public float rBulletScale;

    public float mgForce;
    public float shotForce;
    public float rocketForce;

    public bool isSlowed;

   
    void Awake()
    {
         
        rb = GetComponent<Rigidbody2D>();
        m_anim = GetComponentInChildren<Animator>();
        m_sprite = GetComponentInChildren<SpriteRenderer>();
        m_Audio = GetComponent<AudioSource>();
        SwitchWeapon(weapons.Machinegun);

    }

    void Start()
    {
        shake = FindObjectOfType<CameraScreenShake>();
    }

    void Update()
    {
        if (canMove)
        {
            MovePlayer();
            //TrackMouse();
            FireCall();
        }

        if (Input.GetKeyDown(KeyCode.Mouse1) && buttonMashCount > 0)
        {
            buttonMashCount--;
            if (buttonMashCount == 0)
            {
                GameObject particle = getUpParticle;
                particle = Instantiate(particle, transform.position, Quaternion.identity) as GameObject;
                particle.transform.parent = this.transform;
                rb.isKinematic = false;
                isPlayerDown = false;
                canMove = true;
                target.targetable = true;
                StartCoroutine(Invulnerable());
            }
        }

        if (slowEnemyCount > 0)
        {
            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D))
            {
                wasdCount--;
                if (wasdCount == 0)
                {
                    isSlowed = false; 
                    BroadcastMessage("ShakeOff", SendMessageOptions.DontRequireReceiver);
                    wasdCount = 0;
                    slowEnemyCount = 0;
                }
            }
        }

        pickUpTimer -= Time.deltaTime;
        if (pickUpTimer <= 0)
        {
            pickUpTimer = 0;
            SwitchWeapon(weapons.Machinegun);
        }

        //slowEnemyCount = GetComponentsInChildren<Enemy>().Length;

        if (Input.GetKey(KeyCode.E))
        {
            BroadcastMessage("ShakeOff", SendMessageOptions.DontRequireReceiver);
            slowEnemyCount = 0;
        }

    }

    //Just checks for a horizontal or vertical input from A,S,D,W or ArrowKeys and applys it to the player's position with the asigned speed.
    void MovePlayer()
    {
        float speed = playerMoveSpeed - (0.1f * playerMoveSpeed * slowEnemyCount);
        if (speed < 0) speed = 0;
        float verticalMovement = speed * Input.GetAxis("Vertical");
        float HorizontalMovement = speed * Input.GetAxis("Horizontal");

       
        transform.Translate(HorizontalMovement, verticalMovement, 0, Space.World);

        if (HorizontalMovement < 0)
        {
            m_sprite.flipX = true;
            isPlayerFlipped = true;
        }
        else if (HorizontalMovement > 0)
        {
            m_sprite.flipX = false;
            isPlayerFlipped = false;
        }

        if (verticalMovement != 0 || HorizontalMovement != 0)
        {
            m_anim.Play("WalkAnim");
            footstepTimer += Time.deltaTime;
            if (footstepTimer >= .3f)
            {
                m_Audio.pitch = Random.Range(.9f, 1.1f);
                int randNum = Random.Range(0, footstepSounds.Length);
                m_Audio.PlayOneShot(footstepSounds[randNum], .1f);
                footstepTimer = 0;
            }
        }
        else
        {
            footstepTimer = 0;
        }
    }

    //Checks the current weaponState and adjust accordingly
    public void SwitchWeapon(weapons a_weaponStates)
    {
        weaponStates = a_weaponStates;
        if (weaponStates == weapons.Machinegun)
        {
            machinegun.gameObject.SetActive(true);
            shotgun.gameObject.SetActive(false);
            rocket.gameObject.SetActive(false);
            bullet.GetComponent<SpriteRenderer>().sprite = mBullet;
            bullet.GetComponent<Transform>().localScale = new Vector2(mBulletScale, mBulletScale);
            fireRate = mfireRate;

        }
        else if (weaponStates == weapons.Shotgun)
        {
            machinegun.gameObject.SetActive(false);
            shotgun.gameObject.SetActive(true);
            rocket.gameObject.SetActive(false);
            bullet.GetComponent<SpriteRenderer>().sprite = sBullet;
            bullet.GetComponent<Transform>().localScale = new Vector2(sBulletScale, sBulletScale);
            fireRate = sfireRate;
        }
        else if (weaponStates == weapons.Rocket)
        {
            machinegun.gameObject.SetActive(false);
            shotgun.gameObject.SetActive(false);
            rocket.gameObject.SetActive(true);
            bullet.GetComponent<SpriteRenderer>().sprite = rBullet;
            bullet.GetComponent<Transform>().localScale = new Vector2(rBulletScale, rBulletScale);
            fireRate = rfireRate;
        }
    }

    void TrackMouse()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.rotation = Quaternion.LookRotation(Vector3.forward, mousePos - transform.position);
        //mouseAng = mousePos - (Vector2)transform.position;
    }

    void Shoot()
    {
        //Vector2 shootPoint = GameObject.Find("ShootPoint").transform.position;

        //Screenshake stuff
        shake.Init();
        shake.ScreenShake(.05f, .05f);
        GameObject flash = Instantiate(muzzleFlash, machinegun.shootPos[0].position, machinegun.shootPos[0].transform.rotation) as GameObject;
        flash.transform.parent = this.transform;


        switch (weaponStates)
        {
            case weapons.Machinegun:

                Debug.Log("Shooting 1 Bullet");
                bullet.GetComponent<BulletController>().isRocket = false;
                GameObject mBullet = Instantiate(bullet, machinegun.shootPos[0].position, machinegun.shootPos[0].transform.rotation) as GameObject;
                mBullet.GetComponent<BulletController>().rb.AddForce(machinegun.shootPos[0].transform.up * mBullet.GetComponent<BulletController>().force, ForceMode2D.Impulse);
                m_Audio.PlayOneShot(machineGunSound, .5f);
                fireRate = mfireRate;
                StartCoroutine(Knockback(machinegun.shootPos[0].transform.up,mgForce));
                //Bullet.transform.parent = transform;
                break;

            case weapons.Shotgun:
                Debug.Log("Shooting 3 Bullets");
                bullet.GetComponent<BulletController>().isRocket = false;
                GameObject sBullet = Instantiate(bullet, shotgun.shootPos[0].position, Quaternion.identity) as GameObject;
                GameObject sBullet2 = Instantiate(bullet, shotgun.shootPos[1].position, Quaternion.identity) as GameObject;
                GameObject sBullet3 = Instantiate(bullet, shotgun.shootPos[2].position, Quaternion.identity) as GameObject;
                sBullet.GetComponent<BulletController>().rb.AddForce(shotgun.shootPos[0].transform.up * sBullet.GetComponent<BulletController>().force, ForceMode2D.Impulse);
                sBullet2.GetComponent<BulletController>().rb.AddForce(shotgun.shootPos[1].transform.up * sBullet2.GetComponent<BulletController>().force, ForceMode2D.Impulse);
                sBullet3.GetComponent<BulletController>().rb.AddForce(shotgun.shootPos[2].transform.up * sBullet3.GetComponent<BulletController>().force, ForceMode2D.Impulse);
                m_Audio.PlayOneShot(shotgunSound, .5f);
                fireRate = sfireRate;
                StartCoroutine(Knockback(shotgun.shootPos[0].transform.up, shotForce));
                break;

            case weapons.Rocket:
                Debug.Log("Shooting Rocket");
                bullet.GetComponent<BulletController>().isRocket = true;
                GameObject rocket1 = Instantiate(bullet, rocket.shootPos[0].transform.position, rocket.shootPos[0].transform.rotation) as GameObject;
                rocket1.GetComponent<BulletController>().rb.AddForce(rocket.shootPos[0].transform.up * rocket1.GetComponent<BulletController>().force, ForceMode2D.Impulse);
                m_Audio.PlayOneShot(rocketSound, .5f);
                fireRate = rfireRate;
                StartCoroutine(Knockback(rocket.shootPos[0].transform.up, rocketForce));
                break;
            default:
                break;
        }

        
    }

    void FireCall()
    {
        if (Input.GetKey(KeyCode.Mouse0) && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Shoot();
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.name.Contains("Brute") && canBeHit)
        {
            
            Knockdown();
        }

        else if (other.gameObject.name.Contains("Slow"))
        {
            slowEnemyCount++;
            isSlowed = true; 
            //Debug.Log(slowEnemyCount);
        }

    }

    public void Knockdown()
    {
        isPlayerDown = true;
        rb.isKinematic = true;
        m_anim.Play("Down");
        canMove = false;
        target.targetable = false;
        buttonMashCount = 7;
    }

    IEnumerator Knockback(Vector2 dir, float force)
    {
        rb.AddForce(-dir * force, ForceMode2D.Impulse);
        yield return new WaitForSeconds(0.1f);
        rb.velocity = Vector2.zero;
    }

    IEnumerator Invulnerable()
    {
        canBeHit = false;
        yield return new WaitForSeconds(1f);
        canBeHit = true;

    }

}
