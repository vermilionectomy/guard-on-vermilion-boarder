﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public GameObject player;
    float xOffset;
    float yOffset;
    CameraScreenShake shake;
    private Vector3 veloc = Vector3.zero;
    public float smoothTime;

    bool gameStart = true; 
    bool hasLerped; 
    float camSize;
    EndNarration narrate;

    float startTimer; 

    void Awake()
    {
        narrate = FindObjectOfType<EndNarration>();
        player = GameObject.FindGameObjectWithTag("Player");
        shake = GetComponent<CameraScreenShake>();
        if (shake == null)
        {
            gameObject.AddComponent<CameraScreenShake>();            
        }

        camSize = 12f;

        transform.position = new Vector3(player.transform.position.x, player.transform.position.y, -5);
    }
    //void Update()
    //{
    //    transform.position = (new Vector3(player.transform.position.x + xOffset, player.transform.position.y + yOffset, -5f));
    //}

    void Update()
    {
        startTimer += Time.deltaTime;
        if (narrate.gameStartNow &&  gameStart)
        {
            StartGameLerp(); 
        }

        Camera.main.orthographicSize = camSize;

        //Debug.Log(gameStart);

        
    }

    void StartGameLerp()
    {
        if (!hasLerped && gameStart)
        {
            camSize = Mathf.Lerp(camSize, 5, (1 * Time.deltaTime));
            if (camSize <= 7)
            {               
                gameStart = false;
                hasLerped = true;
            }
        }
    }

    void LateUpdate()
    {
        if(gameStart)
        {
            transform.position = Vector3.SmoothDamp(transform.position, (new Vector3(player.transform.position.x + xOffset, player.transform.position.y + yOffset, -5f)), ref veloc, smoothTime);
        }
        if(!gameStart)
        {
            transform.position = Vector3.SmoothDamp(transform.position, (new Vector3(player.transform.position.x + xOffset, player.transform.position.y + yOffset, -5f)), ref veloc, smoothTime);
            float zPos = transform.position.z;
            zPos = -5f;
        }            
    }

}
