﻿using UnityEngine;
using System.Collections;

public class ExplosionController : MonoBehaviour {

    public float explosionTimer;
    public AudioClip expSound;
    public AudioClip weedSound;
    AudioSource m_Audio;
    public bool weed;

    void Start ()
    {
        m_Audio = GetComponent<AudioSource>();
        if (weed)
        {
            m_Audio.PlayOneShot(weedSound, .5f);
        }
        else
        {
            m_Audio.PlayOneShot(expSound, .5f);
        }
    }
	
	void Update ()
    {
        explosionTimer -= Time.deltaTime;
        if (explosionTimer <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            other.GetComponent<Enemy>().LoseHealth(5);
        }
    }
}
