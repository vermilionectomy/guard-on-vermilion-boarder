﻿using UnityEngine;
using System.Collections;

public class EnemyHit : MonoBehaviour
{
    SpriteRenderer m_Sprite;
    bool hasHit;
    bool lerpNow;
    float timer;
    void Awake()
    {
        m_Sprite = GetComponent<SpriteRenderer>();
    }

    public void EnemyFlash()
    {
        if (!hasHit)
        {
            lerpNow = true;
            hasHit = false;
        }
    }

    void Update()
    {
        if (lerpNow)
        {           
            m_Sprite.color = Color.red;
            timer += Time.deltaTime;
            if(timer >= .1f)
            {
                m_Sprite.color = Color.white;
                timer = 0;
            }                   

            if (m_Sprite.color == Color.white)
            {
                hasHit = false;
                lerpNow = false;
            }
        }
    }
    
}
