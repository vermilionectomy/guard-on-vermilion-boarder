﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MaterialOpacity : MonoBehaviour
{
    private SpriteRenderer sp;
    private Image im;

    //Current color RGB values (Dont change)
    private float sp_r;
    private float sp_g;
    private float sp_b;

    public bool isFading;

    //Opacity of object 
    [SerializeField]
    [Range(0, 1)]
    private float opacity;

    [SerializeField]
    private bool isSprite;

    float timeToDie = 1;
    void Awake()
    {
        if (isSprite)
        {
            sp = GetComponent<SpriteRenderer>();

            sp_r = sp.color.r;
            sp_g = sp.color.g;
            sp_b = sp.color.b;

            sp.color = new Color(sp_r, sp_g, sp_b, opacity);
        }

        else if (!isSprite)
        {
            im = GetComponent<Image>();

            sp_r = im.color.r;
            sp_g = im.color.g;
            sp_b = im.color.b;


            im.color = new Color(sp_r, sp_g, sp_b, opacity);
        }


    }

    //void Update()
    //{
    //    if (isSprite)
    //        //Visualization number (TAKE OUT LATER)
    //        sp.color = new Color(sp_r, sp_g, sp_b, opacity);

    //    else if (!isSprite)
    //        im.color = new Color(sp_r, sp_g, sp_b, opacity);


    //    if (isFading)
    //    {
            

    //        timeToDie -= Time.deltaTime;
    //        opacity = timeToDie;
    //        if (opacity <= 0)
    //        {
    //            Destroy(this.gameObject);

    //        }
    //    }

    //}

}