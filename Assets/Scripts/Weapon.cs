﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour
{
    public float fireRate;
    public Transform[] shootPos;

    public SpriteRenderer gunSprite;

    // Use this for initialization
    void Start()
    {
        gunSprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Camera.main.ScreenToWorldPoint(Input.mousePosition).x < transform.position.x)
        {
            gunSprite.flipY = true;
            Debug.Log("gunsprite flip = " + gunSprite.flipX);
        }
        else
            gunSprite.flipY = false;


       
    }
}
