﻿using UnityEngine;
using System.Collections;

using UnityEngine;
using System.Collections;

public class PickupController : MonoBehaviour
{
    public bool Shotgun;
    public bool Rocket;

    AudioSource m_audio;
    public AudioClip pickupSound;
    float timer;
    bool pickedUp;
    SpriteRenderer m_sprite; 
    void Start()
    {
        m_audio = GetComponent<AudioSource>();
        m_sprite = GetComponentInChildren<SpriteRenderer>(); 
    }

    void Update()
    {
        //if(pickedUp)
        //{
        //    timer += Time.deltaTime;
        //    if (timer >= .7f)
        //    {
        //        Destroy(this.gameObject);
        //    }
        //}
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (Shotgun)
            {
                other.GetComponent<CharacterController2D>().SwitchWeapon(weapons.Shotgun);
                other.GetComponent<CharacterController2D>().pickUpTimer = 20.0f;
                m_audio.PlayOneShot(pickupSound, .3f);
                m_sprite.enabled = false;
                pickedUp = true;
                Destroy(this.gameObject);

            }
            else
            {
                if (Rocket)
                {
                    other.GetComponent<CharacterController2D>().SwitchWeapon(weapons.Rocket);
                    other.GetComponent<CharacterController2D>().pickUpTimer = 20.0f;
                    m_audio.PlayOneShot(pickupSound, .1f);
                    pickedUp = true;
                    m_sprite.enabled = false;
                    Destroy(this.gameObject);
                }
            }

        }
    }
}
