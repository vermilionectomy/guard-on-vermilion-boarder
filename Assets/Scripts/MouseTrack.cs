﻿using UnityEngine;
using System.Collections;

public class MouseTrack : MonoBehaviour {

    SpriteRenderer m_sprite;
    CharacterController2D player;

    void Awake()
    {
        player = FindObjectOfType<CharacterController2D>(); 
        m_sprite = GetComponent<SpriteRenderer>(); 
    }

    void Update()
    {
        TrackMouse(); 

        //if(player.isPlayerFlipped)
        //{
        //    m_sprite.flipX = true;
        //}
        //else
        //{
        //    m_sprite.flipX = false; 
        //}
    }

    void TrackMouse()
    {
       //Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
       //transform.rotation = Quaternion.LookRotation(Vector3.forward,mousePos-transform.position);
        //transform.localEulerAngles = new Vector3(mousePos.x, mousePos.y, mousePos.z); 

         // convert mouse position into world coordinates
 Vector2 mouseScreenPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
 
 // get direction you want to point at
 Vector2 direction = (mouseScreenPosition - (Vector2) transform.position).normalized;
 
 // set vector of transform directly
 transform.right = direction;
    }
}
