﻿using UnityEngine;
using System.Collections;

public class PlayerHitFlash : MonoBehaviour {

    CharacterController2D player;

    bool gettingUp;
    bool timeToFlash;
    SpriteRenderer m_sprite;
    float timer;


    bool hasDone;
    void Start()
    {
        player = FindObjectOfType<CharacterController2D>();
        m_sprite = GetComponent<SpriteRenderer>();
    }


    void Update()
    {
        if (player.isPlayerDown && !timeToFlash)
        {
            timeToFlash = true;
        }

        if (timeToFlash && !player.isPlayerDown)
        {
            FlashNow();
        }
    }

    void FlashNow()
    {
        if (!hasDone)
        {
            StartCoroutine(ColorChange());
            hasDone = true;
        }

        timer += Time.deltaTime;
        if (timer >= 2)
        {
            m_sprite.color = Color.white;            
            timeToFlash = false;
            timer = 0;
            ResetVars();
            
        }
    }


    IEnumerator ColorChange()
    {
        m_sprite.color = new Color(1, 1, 1, .5f);
        yield return new WaitForSeconds(.3f);
        m_sprite.color = new Color(1, 1, 1, 1f);
        yield return new WaitForSeconds(.3f);
        m_sprite.color = new Color(1, 1, 1, .5f);
        yield return new WaitForSeconds(.3f);
        m_sprite.color = new Color(1, 1, 1, 1f);
        yield return new WaitForSeconds(.3f);
        hasDone = false;
    }

    void ResetVars()
    {
        hasDone = false;
        timeToFlash = false;
        hasDone = false; 
    }
}
