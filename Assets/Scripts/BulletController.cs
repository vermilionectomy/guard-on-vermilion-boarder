﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BulletController : MonoBehaviour {

    public float lifeTime;
    public float force;
    public Rigidbody2D rb;
    public bool isRocket;
    public GameObject explosion;
    public ParticleSystem explosionPar;
    public List<GameObject> splatterEffects = new List<GameObject>();


    public GameObject bloodEffect;
    public ParticleSystem sparkGameobject;

    public int damage;

	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
    }
	
	void Update ()
    {
        //transform.Translate(new Vector2(0, force));
        //transform.Translate(transform.right * force);
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0)
        {
            if (isRocket)
            {
                Instantiate(explosion, transform.position, Quaternion.identity);
                Destroy(this.gameObject);
            }
            else
            Destroy(this.gameObject);
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (splatterEffects.Count != 0)
        {
            int ranSplatterEffect = Random.Range(0, splatterEffects.Count);
            float ranSplatterRotate = Random.Range(-360, 360);
            //Debug.Log(ranSplatterRotate);            
           // Instantiate(splatterEffects[ranSplatterEffect], other.transform.position, new Quaternion(transform.rotation.x, transform.rotation.y, ranSplatterRotate, 0)); 
           //Instantiate(bloodEffect, new Vector3(transform.position.x, transform.position.y, -.01f), Quaternion.Euler(0.0f, 0.0f, Random.Range(0.0f, 360.0f)));
        }
        if (isRocket)
        {
            Instantiate(explosion, transform.position, Quaternion.identity);
            Instantiate(explosionPar, transform.position, Quaternion.identity);
            explosionPar.Emit(1);
            Destroy(this.gameObject);
        }
        else
            Destroy(gameObject);

        if(other.gameObject.CompareTag("Wall"))
        {
            Instantiate(sparkGameobject, transform.position, transform.rotation);
            sparkGameobject.Emit(1); 
        }
    }
}
