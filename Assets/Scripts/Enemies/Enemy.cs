﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Enemy : MonoBehaviour
{
    public Image indicator;

    public float attackRange;
    public enum State
    {
        move,
        attack,
        slow,
        
    }
    public State m_state;

    public Target m_target;

    public GameObject bloodEffect;


    public int m_health;

    public float m_moveSpeed;
    public float m_attackSpeed;

    public bool targetPlayer;
    public bool slowPlayer;

    public bool attackingWall;
    public Wall wall;

    Vector2 stickDir;

    AudioSource m_Audio;
    public AudioClip deathSound;
    public AudioClip hitSound;

    EnemyHit hit;

    public GameObject hitEffect; 

    public GameObject shotgunPickup;
    public GameObject rocketPickup;

    void Awake()
    {
        hit = GetComponentInChildren<EnemyHit>(); 
        if (m_Audio == null)
        {
            gameObject.AddComponent<AudioSource>();
        }

        m_Audio = GetComponent<AudioSource>();

        
    }

    // Use this for initialization
    void Start()
    {
        //m_target = m_targets[0].GetComponent<Target>();
    }

    // Update is called once per frame
    public void Update()
    {
        if (GameManager.Instance.m_playerTarget.targetable && targetPlayer)
            m_target = GameManager.Instance.m_playerTarget;

        if (!m_target)
        {
            m_target = GameManager.Instance.m_wallTargets[Random.Range(0, GameManager.Instance.m_wallTargets.Count)];
        }

        if(!m_target)
        {
            return;
        }

       

        switch (m_state)
        {
            case State.move:
                Move(m_moveSpeed, m_target);
                if (Vector2.Distance(this.transform.position, m_target.transform.position) < attackRange)
                    m_state = State.attack;
                break;
            case State.attack:
                Attack(m_target);
                if (Vector2.Distance(this.transform.position, m_target.transform.position) > attackRange)
                    m_state = State.move;

                break;
            case State.slow:
                Rigidbody2D rb = GetComponent<Rigidbody2D>();
                rb.isKinematic = false;
                gameObject.layer = 11;
                break;
        }
        //Move(m_speed, m_target);

        if (!m_target.targetable)
        {
            m_target = null;
        }

    }

    public void Move(float a_moveSpeed, Target a_target)
    {
        transform.position = Vector2.MoveTowards(transform.position, a_target.gameObject.transform.position, a_moveSpeed * Time.deltaTime);
    }

    public void LoseHealth(int a_damage)
    {
        hit.EnemyFlash();
        Instantiate(hitEffect, transform.position, transform.rotation); 
        m_health--;
        if (m_health == 0)
        {
            Death();
        }
    }

    public void Death()
    {
        GameManager.Instance.numEnemies--;
        m_Audio.PlayOneShot(deathSound, .2f);
        Instantiate(bloodEffect, new Vector3(transform.position.x, transform.position.y, -.01f), Quaternion.Euler(0.0f, 0.0f, Random.Range(0.0f, 360.0f)));
        int ranChance = Random.Range(0, 10);
        if (ranChance >= 5)
        {
            int ranNum = Random.Range(0, 2);
            if (ranNum == 0)
            {
                Instantiate(shotgunPickup, transform.position, Quaternion.identity);
            }
            else
            {
                Instantiate(rocketPickup, transform.position, Quaternion.identity);
            }
        }
        if (attackingWall)
        {
            wall.enemyCount--;
        }
        Destroy(indicator.gameObject);
        Destroy(this.gameObject);
        
       
    }

    public void GetTarget()
    {

    }

    void Attack(Target a_target)
    {
        if (slowPlayer)
        {

        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log("ouch");
        if (other.gameObject.CompareTag("Bullet"))
        {
            LoseHealth(other.gameObject.GetComponent<BulletController>().damage);
            m_Audio.PlayOneShot(hitSound, 0.1f);
        }

        else if (other.gameObject.CompareTag("Player") && slowPlayer)
        {
            m_state = State.slow;
            gameObject.transform.parent = other.gameObject.transform;
            other.gameObject.GetComponent<CharacterController2D>().wasdCount += 4;
            stickDir = other.contacts[0].point - (Vector2)transform.position;
            

        }
    }

    IEnumerator ShakeOff()
    {
        stickDir = -stickDir.normalized;
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.AddForce(stickDir * 2000f);
        transform.parent = null;
        yield return new WaitForSeconds(0.5f);
        rb.velocity = Vector2.zero;
        rb.isKinematic = true;
        gameObject.layer = 10;
        m_state = State.move;  
    }


}
