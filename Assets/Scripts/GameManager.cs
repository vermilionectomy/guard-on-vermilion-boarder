﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public List<Target> m_wallTargets;
    public Target m_playerTarget;

    public int remainingWalls;

    public Dictionary<int, int[]> waveInfo = new Dictionary<int, int[]>();

    public string fileName = "WaveData.csv";
    public string filePath;

    bool wavecanstart = true;
    public List<GameObject> enemies = new List<GameObject>();

    public int waveNum;
    public int numEnemies;

    public GameObject wallEnemy;
    public GameObject bruteEnemy;
    public GameObject slowEnemy;

    public GameObject[] spawnPoints;

    public GameObject waveStartText;

    public EndNarration en;

    GameObject uiCanvas;

    public Transform waveStartTransform; 

    void Awake()
    {
        uiCanvas = GameObject.Find("Main Canvas"); 
        if (Instance != null)
            GameObject.Destroy(Instance);
        else
            Instance = this;

        filePath = Application.dataPath + "/" + fileName;

        if (ImportWaveData(filePath))
        {
            Debug.Log("wave data imported");
        }

    }
    // Use this for initialization
    void Start()
    {
        remainingWalls = m_wallTargets.Count;
    }

    // Update is called once per frame
    void Update()
    {
        if (numEnemies == 0 && wavecanstart && en.gameStartNow)
        {

            StartCoroutine("NextWave");
            waveNum++;
            wavecanstart = false;
            //Debug.Log("start wave " +waveNum);
        }

        if (remainingWalls == 0)
        {
            Application.LoadLevel(2); 
            //Debug.Log("Game Over");
        }
    }

    bool ImportWaveData(string file)
    {
        if (!File.Exists(file))
        {
            return false;
        }

        string line = string.Empty;
        string[] data;

        StreamReader reader = new StreamReader(file);

        using (reader)
        {
            do
            {
                line = reader.ReadLine();
                if (line != null)
                {
                    data = line.Split(',');
                    if (!string.IsNullOrEmpty(data[0]) || !string.IsNullOrEmpty(data[1]) || !string.IsNullOrEmpty(data[2]) || !string.IsNullOrEmpty(data[3]))
                    {
                        int waveNum = int.Parse(data[0]);
                        int wallEnemies = int.Parse(data[1]);
                        int bruteEnemies = int.Parse(data[2]);
                        int slowEnemies = int.Parse(data[3]);

                        waveInfo.Add(waveNum, new int[] { wallEnemies, bruteEnemies, slowEnemies });

                    }
                    data = new string[0];

                }


            } while (line != null);

            reader.Close();

        }
        return true;
    }

    void SpawnWave(int waveNum)
    {
        int[] waveEnemies = Get(waveNum);
        int numWall = waveEnemies[0];
        int numBrute = waveEnemies[1];
        int numSlow = waveEnemies[2];
        numEnemies = numWall + numBrute + numSlow;

        //Debug.Log(numWall + " wall, " + numBrute + " brutes and" + numSlow + " slow spawned");
        for (int i = 0; i < numWall; i++)
        {
            Instantiate(wallEnemy, spawnPoints[Random.Range(0, spawnPoints.Length)].transform.position, wallEnemy.transform.rotation);

        }

        for (int i = 0; i < numBrute; i++)
        {
            //spawn ranged at spawn points
            Instantiate(bruteEnemy, spawnPoints[Random.Range(0, spawnPoints.Length)].transform.position, bruteEnemy.transform.rotation);

        }

        for (int i = 0; i < numSlow; i++)
        {
            //spawn ranged at spawn points
            Instantiate(slowEnemy, spawnPoints[Random.Range(0, spawnPoints.Length)].transform.position, slowEnemy.transform.rotation);

        }
        //CheckEnemies();


       // Debug.Log("got here");
    }

    public int[] Get(int waveNum)
    {
        int[] temp = new int[0];
        waveInfo.TryGetValue(waveNum, out temp);
        return temp;
    }

    IEnumerator NextWave()
    {
        yield return new WaitForSeconds(2f);
        //nextWave.GetComponent<Animator>().Play("WaveStartAnim");
        GameObject wave = Instantiate(waveStartText);
        wave.transform.position = waveStartTransform.transform.position; 
 
        //nextWaveAudio.GetComponent<WaveStartSound>().StartCoroutine("playAudio");
        yield return new WaitForSeconds(3f);
        SpawnWave(waveNum);
        wavecanstart = true;
    }
}
