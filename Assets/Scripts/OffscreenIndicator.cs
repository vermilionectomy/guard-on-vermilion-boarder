﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OffscreenIndicator : MonoBehaviour
{



    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void LateUpdate()
    {

        Indicate();

    }

    void Indicate()
    {
        Enemy[] enemies = GameObject.FindObjectsOfType(typeof(Enemy)) as Enemy[];

        foreach (Enemy e in enemies)
        {
            if (e.attackingWall)
            {
                Vector3 screenPos = Camera.main.WorldToScreenPoint(e.transform.position);

                if (screenPos.x > 0 && screenPos.x < Screen.width && screenPos.y > 0 && screenPos.y < Screen.height)
                {
                    e.indicator.gameObject.SetActive(false);
                    //Debug.Log("ONSCREEN"+e.gameObject.name);
                    e.indicator.gameObject.transform.SetParent(e.gameObject.transform);
                    
                    continue;
                }

                Vector3 screenCenter = new Vector3(Screen.width, Screen.height, 0) / 2;

                screenPos -= screenCenter;

                float angle = Mathf.Atan2(screenPos.y, screenPos.x);
                angle -= 90 * Mathf.Deg2Rad;

                float cos = Mathf.Cos(angle);
                float sin = -Mathf.Sin(angle);

                screenPos = screenCenter + new Vector3(sin * 150f, cos * 150f, 0);

                float m = cos / sin;

                Vector3 screenBounds = screenCenter * 0.6f;

                if (cos > 0)
                {
                    screenPos = new Vector3(screenBounds.y / m, screenBounds.y, 0);
                }

                else
                {
                    screenPos = new Vector3(-screenBounds.y / m, -screenBounds.y, 0);
                }

                if (screenPos.x > screenBounds.x)
                {
                    screenPos = new Vector3(screenBounds.x, screenBounds.x * m, 0);
                }

                else if (screenPos.x < -screenBounds.x)
                {
                    screenPos = new Vector3(-screenBounds.x, -screenBounds.x * m, 0);
                }

                //screenPos += screenCenter;

               // Debug.Log("OFFSCREEN" + e.gameObject.name);

                e.indicator.gameObject.SetActive(true);
                e.indicator.gameObject.transform.SetParent(this.gameObject.transform);
                e.indicator.gameObject.transform.localPosition = screenPos;
                e.indicator.gameObject.transform.localRotation = Quaternion.Euler(0, 0, angle * Mathf.Rad2Deg);

            }
        }
    }
}
