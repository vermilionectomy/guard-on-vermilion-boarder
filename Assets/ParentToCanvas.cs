﻿using UnityEngine;
using System.Collections;

public class ParentToCanvas : MonoBehaviour {

    GameObject UI;  
	void Start()
    {
        UI = GameObject.Find("Main Canvas"); 
        transform.parent = UI.transform; 
    }
}
